# Federation Proxy

The Federation Proxy provides seamless cross-domain authentication/authorization for web browser access and cross-domain API calls (B2B) between two tenants in Kubernetes environment, enabled by Service Mesh setup, a Keycloak-integrated gateway developed for Fogprotect, and Keycloak beta feature for inter-Keycloak token exchange. More precisely, it enables any "legacy" app from one security domain (organization A) to call any federated external domain (org. B)'s service APIs securely (in compliance with access control policy) without any app adaptation or integration. It uses state-of-the-art OpenID Connect and latest RFC 8693 standard compliance.

## Preliminary setup of the two domains' Keycloak servers
Let's assume one domain is Chatterbox Manager domain and another is Story Maker domain (like in UC3 Smart Media).

1. Create the Client `chatterbox-idp` in the Story Maker's Keycloak (realm `story-maker-realm`) and configure it. [More info](https://www.keycloak.org/docs/latest/securing_apps/#external-token-to-internal-token-exchange).
1. Create the Client `storymaker-wp6-egress-proxy` in the Chatterbox Manager's Keycloak (realm `Chatterbox_Manager`) and configure it properly. [More info](https://www.keycloak.org/docs/latest/securing_apps/#external-token-to-internal-token-exchange).
1. Register the Story Maker's Identity Provider (IdP) in the Chatterbox Manager's Keycloak's (trusted) Identity Providers (realm `Chatterbox_Manager`) and configure it properly with role mappings if needed, and hardcoded organization attribute, and permissions granted to `storymaker-wp6-egress-proxy` for token exchange [more info](https://www.keycloak.org/docs/latest/securing_apps/#_grant_permission_external_exchange). Currently registered in Chatterbox Manager's Keycloak as `ATC (Story Maker)'s Identity Provider`.
1. Register the Client `wp4-proxy` in the Chatterbox Manager's Keycloak (realm `Chatterbox_Manager`) and configure it properly with `realm_roles` client scope, a Protocol Mapper for the organization _User Attribute_, and access permission granted to `storymaker-wp6-egress-proxy`. [More info](https://www.keycloak.org/docs/latest/securing_apps/#_client_to_client_permission).


## Deploying the Federation Proxy

1. Create the proxy ConfigMap from the application.yml on this repository after configuring properly:
   - Backend URL: next endpoint to forward request to (e.g. Istio ingress gateway in UC3)
   - Keycloak/OIDC settings: Target domain's Keycloak Token Endpoint URI, ClientId, ClientSecret, subject tken issuer alias, audience, token cache settings

   ```
   $ kubectl apply -f application.yml
   $ kubectl describe configmap wp6-egress-gateway  -n story-maker
   ```

1. Retrieve `Deployment_and_Service_wp6-egress-gateway.yaml` from this repository and modify/replace:
   - Host/IP of external Keycloak in `hostAliases`

   ```
   $ kubectl apply -f Deployment_and_Service_wp6-egress-gateway.yaml
   ```

1. Make sure an Istio sidecar proxy is injected in each Pods, Deployments, etc. of the client applications that with to call the external domain. For example, to force Istio injection in a specific Deployment `storymaker-be`:
  ```
  $ kubectl get deployment storymaker-be -n story-maker -o yaml | istioctl kube-inject -f - | kubectl apply -f -
  ```
1. Apply the Istio VirtualService `proxy-to-wp6-egress-gw` for Istio sidecar's request routing to the gateway by using the file `VirtualService_proxy_to_wp6_egress_gw.yaml ` on this git repository:

   ```
   $ kubectl apply -f VirtualService_proxy_to_wp6_egress_gw.yaml
   $ kubectl describe virtualservice proxy-to-wp6-egress-gw  -n story-maker
   ```

1. Verify that the federation proxy is properly running. Check the logs:
   ```
   $ kubectl logs --follow=true wp6-egress-gateway-8d9857cd6-jddk4  -n story-maker
   ```

1. In order to change the federation proxy's configuration at runtime:

   ```
   $ kubectl edit configmap wp6-egress-gateway
   $ kubectl -n story-maker rollout restart deployment wp6-egress-gateway
   ```
